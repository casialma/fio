!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
! *****************************************************************************
!
! File: text_file.f90
!
! Author: Casimiro Álvarez Mariño
!
! Module: text_file_class
!
! PURPOSE: clase que define las rutinas para escribir y leer archivos de texto.
!
! Created Date: 03/05/2017
! Modified Date: 14/06/2017
!
! *****************************************************************************
!
module fio_text_file_class
    implicit none
    private
    public :: fio_text_file_type, new_fio_text_file_reader, new_fio_text_file_writer

    integer, parameter, public :: fio_file_path_len = 100
    integer, parameter, public :: fio_file_line_len = 256
    character(len = 8), parameter, public :: fio_file_string_format = '(A256)'

    character(len = 1), parameter  :: fio_file_sep_symbol = ';'
    character(len = 8), parameter  :: fio_file_integer_format = '(I5)'
    character(len = 8), parameter  :: fio_file_real_format = '(ES13.6)'
    character(len = 8), parameter  :: fio_file_read_sep_format = '(1X)'
    character(len = 8), parameter  :: fio_file_write_sep_format = '(A1)'
    character(len = 8), parameter  :: fio_file_read_integer_with_sep_format = '(I5, 1X)'
    character(len = 8), parameter  :: fio_file_write_integer_with_sep_format = '(I5, A1)'
    character(len = 12), parameter :: fio_file_read_real_with_sep_format = '(ES13.6, 1X)'
    character(len = 12), parameter :: fio_file_write_real_with_sep_format = '(ES13.6, A1)'

    integer :: fio_file_unit = 20

    type fio_text_file_type
        integer :: fio_file_unit
        character(len = fio_file_path_len) :: file_path
    contains
        procedure, public, pass :: to_close => fio_text_file_to_close
        procedure, public, pass :: advance_line => fio_text_file_advance_line
        procedure, public, pass :: read_line => fio_text_file_read_line
        procedure, public, pass :: read_integer => fio_text_file_read_integer
        procedure, public, pass :: read_integer_with_separator => fio_text_file_read_integer_with_sep
        procedure, public, pass :: read_real => fio_text_file_read_real
        procedure, public, pass :: read_real_with_separator => text_file_read_real_with_separator
        procedure, public, pass :: write_line => fio_text_file_write_line
        procedure, public, pass :: write_integer => fio_text_file_write_integer
        procedure, public, pass :: write_integer_with_separator => fio_text_file_write_integer_with_sep
        procedure, public, pass :: write_real => fio_text_file_write_real
        procedure, public, pass :: write_real_with_separator => fio_text_file_write_real_with_sep
    end type fio_text_file_type


    interface new_fio_text_file_reader
        module procedure :: fio_text_file_reader_new
    end interface

    interface new_fio_text_file_writer
        module procedure :: fio_text_file_writer_new
    end interface

contains
    function fio_text_file_reader_new(file_path, ierr) result(this)
        character(len = fio_file_path_len), intent(in) :: file_path
        integer, intent(inout) :: ierr
        type(fio_text_file_type) :: this
        ierr = 0
        fio_file_unit = fio_file_unit + 1
        open(unit = fio_file_unit, file = file_path, action = "read", status = "old", iostat = ierr)
        if (ierr .gt. 0) then
            write(*,*) 'error al abrir el fichero: ', file_path
            stop
        endif
        this%fio_file_unit = fio_file_unit
        this%file_path = file_path
        return
    end function

    function fio_text_file_writer_new(file_path, ierr, append) result(this)
        character(len = fio_file_path_len), intent(in) :: file_path
        integer, intent(inout) :: ierr
        logical, optional, intent(in) :: append
        type(fio_text_file_type) :: this
        logical :: ok
        ierr = 0
        this%fio_file_unit = 0
        fio_file_unit = fio_file_unit + 1
        ok = .false.
        if (present(append) .and. append) ok = .true.
        if (ok) then
            open(unit = fio_file_unit, file = file_path, action = "write", status = "old", position = 'append', iostat = ierr)
        else
            open(unit = fio_file_unit, file = file_path, action = "write", status = "replace", position = 'rewind', iostat = ierr)
        endif
        if (ierr .gt. 0) then
            open(unit = fio_file_unit, file = file_path, action = "write", status = "new", position = 'rewind', iostat = ierr)
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error al abrir el fichero: ', file_path
            fio_file_unit = fio_file_unit - 1
            return
        endif
        this%fio_file_unit = fio_file_unit
        this%file_path = file_path
        return
    end function

    subroutine fio_text_file_to_close(self)
        class(fio_text_file_type), intent(inout) :: self
        fio_file_unit = fio_file_unit - 1
        close(unit = self%fio_file_unit)
        return
    end subroutine

    function fio_text_file_advance_line(self) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        integer :: ierr
        read(fio_file_unit, '(A1)', iostat = ierr, advance = 'yes')
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
        end if
        return
    end function

    function fio_text_file_read_line(self, value) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        character(len = fio_file_line_len), intent(inout) :: value
        integer :: ierr
        read(fio_file_unit, fio_file_string_format, iostat = ierr, advance = 'yes') value
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_read_integer(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        integer, intent(inout) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            read(fio_file_unit, fio_file_integer_format, iostat = ierr, advance = 'yes') value
        else
            read(fio_file_unit, fio_file_integer_format, iostat = ierr, advance = 'no') value
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_read_integer_with_sep(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        integer, intent(inout) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            read(fio_file_unit, fio_file_read_integer_with_sep_format, iostat = ierr, advance = 'yes') value
        else
            read(fio_file_unit, fio_file_read_integer_with_sep_format, iostat = ierr, advance = 'no') value
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_read_real(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        real, intent(inout) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            read(fio_file_unit, fio_file_real_format, iostat = ierr, advance = 'yes') value
        else
            read(fio_file_unit, fio_file_real_format, iostat = ierr, advance = 'no') value
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function text_file_read_real_with_separator(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        real, intent(inout) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            read(fio_file_unit, fio_file_read_real_with_sep_format, iostat = ierr, advance = 'yes') value
        else
            read(fio_file_unit, fio_file_read_real_with_sep_format, iostat = ierr, advance = 'no') value
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la lectura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_write_line(self, value) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        character(len = fio_file_line_len), intent(in) :: value
        integer :: ierr
        write(fio_file_unit, fio_file_string_format, iostat = ierr, advance = 'yes') adjustl(value)
        if (ierr .gt. 0) then
            write(*,*) 'error en la escritura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_write_integer(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        integer, intent(in) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            write(fio_file_unit, fio_file_integer_format, iostat = ierr, advance = 'yes') value
        else
            write(fio_file_unit, fio_file_integer_format, iostat = ierr, advance = 'no') value
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la escritura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_write_integer_with_sep(self, value, advance_flag) result(ierr)
        class(fio_text_file_type), intent(in) :: self
        integer, intent(in) :: value
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            write(fio_file_unit, fio_file_write_integer_with_sep_format, iostat = ierr, advance = 'yes') value, fio_file_sep_symbol
        else
            write(fio_file_unit, fio_file_write_integer_with_sep_format, iostat = ierr, advance = 'no') value, fio_file_sep_symbol
        endif
        if (ierr .gt. 0) then
            write(*,*) 'error en la escritura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_write_real(self, va, advance_flag) result(ierr)
        class(fio_text_file_type), intent(inout) :: self
        real, intent(in) :: va
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            write(fio_file_unit, fio_file_real_format, iostat = ierr, advance = 'yes') va
        else
            write(fio_file_unit, fio_file_real_format, iostat = ierr, advance = 'no') va
        endif
        if (ierr .gt. 0) then
           	write(*,*) 'error en la escritura del fichero: ', self%file_path
            stop
        end if
        return
    end function

    function fio_text_file_write_real_with_sep(self, va, advance_flag) result(ierr)
        class(fio_text_file_type), intent(inout) :: self
        real, intent(in) :: va
        logical, intent(in) :: advance_flag
        integer :: ierr
        if (advance_flag) then
            write(fio_file_unit, fio_file_write_real_with_sep_format, iostat = ierr, advance = 'yes') va, fio_file_sep_symbol
        else
            write(fio_file_unit, fio_file_write_real_with_sep_format, iostat = ierr, advance = 'no') va, fio_file_sep_symbol
        endif
        if (ierr .gt. 0) then
           	write(*,*) 'error en la escritura del fichero: ', self%file_path
            stop
        end if
        return
    end function
end module fio_text_file_class
