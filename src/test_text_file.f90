!
! Copyright (c) 2017 Casimiro Alvarez Mariño.
! Distributed under terms of the MIT license.
!
! *****************************************************************************
!
! File: test_text_file.f90
!
! Author: Casimiro Álvarez Mariño
!
! Program: test_text_file
!
! PURPOSE: progrma para testear la escritura y lectura de archivos de texto.
!
! Created Date: 03/05/2017
! Modified Date: 14/06/2017
!
! *****************************************************************************
!
program test_text_file
    use fio_text_file_class
    implicit none
    integer :: ierr
    type(fio_text_file_type) :: file
    character(len = fio_file_path_len) :: file_path
    file_path = './'
    file = new_fio_text_file_reader(file_path, ierr)
    write(*,*) ' implementar el test para manipular archivos de texto'
    stop "stop test_text_file"
end program test_text_file
