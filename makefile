#!/usr/bin/make


# defaults
STATIC = yes
COMPILER = gnu

#main building variables
DOBJ = build/obj/
DMOD = build/mod/
DEXE = bin/
DLIB = lib/
DSRC = src/
ifeq "$(STATIC)" "yes"
  MAKELIB = ar -rcs $(DLIB)libfio.a $(DOBJ)*.o $(DMOD)*.mod
  RULE    = fio
else
  RULE = $(DEXE)test_text_file
  ARX = $(DLIB)libfio.a
endif
LIBSEXT = $(DLIB)libfio.a
ifeq "$(COMPILER)" "gnu"
  FC    = gfortran
  OPTSC = -cpp -c -frealloc-lhs -O2 -J $(DMOD)
  OPTSL = -J $(DMOD)
endif
VPATH   = $(DSRC) $(DOBJ) $(DMOD)
MKDIRS  = $(DOBJ) $(DMOD) $(DEXE) $(DLIB)
LCEXES  = $(shell echo $(EXES) | tr '[:upper:]' '[:lower:]')
EXESPO  = $(addsuffix .o,$(LCEXES))
EXESOBJ = $(addprefix $(DOBJ),$(EXESPO))

#auxiliary variables
COTEXT = "Compile $(<F)"
LITEXT = "Assemble $@"
RUTEXT = "Executed rule $@"

firsrule: $(RULE)

#building rules
$(DEXE)test_text_file: $(MKDIRS) $(ARX) $(DOBJ)test_text_file.o
	@rm -f $(filter-out $(DOBJ)test_text_file.o,$(EXESOBJ))
	@echo $(LITEXT)
	@$(FC) $(OPTSL) $(DOBJ)*.o $(LIBSEXT) -o $@
EXES := $(EXES) test_text_file

fio: $(MKDIRS) $(DOBJ)fio.o
	@echo $(LITEXT)
	@$(MAKELIB)
LIBS := $(LIBS) libfio.a

#compiling rules
$(DOBJ)fio.o: src/text_file.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

$(DOBJ)test_text_file.o: src/test_text_file.f90
	@echo $(COTEXT)
	@$(FC) $(OPTSC)  $< -o $@

#phony auxiliary rules
.PHONY : $(MKDIRS)
$(MKDIRS):
	@mkdir -p $@
.PHONY : $(ARX)
$(ARX):
	@ar -x $@
	@rm -f *.o
	@mv *.mod $(DMOD)
.PHONY : cleanobj
cleanobj:
	@echo deleting objects
	@rm -fr $(DOBJ)
.PHONY : cleanmod
cleanmod:
	@echo deleting mods
	@rm -fr $(DMOD)
.PHONY : cleanexe
cleanexe:
	@echo deleting exes
	@rm -f $(addprefix $(DEXE),$(EXES))
cleanlib:
	@echo deleting libs
	@rm -f $(addprefix $(DLIB),$(LIBS))
.PHONY : clean
clean: cleanobj cleanmod
.PHONY : cleanall
cleanall: clean cleanexe cleanlib
